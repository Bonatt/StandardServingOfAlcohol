import math
import random
import numpy as np
import ROOT

# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
  quit()
# Redefine np.sqrt() to sqrt()
def sqrt(x):
  return math.sqrt(x)
def sin(x):
  return math.sin(x)
def cos(x):
  return math.cos(x)
# Redefine pi to something shorter
pi = math.pi
print ''


#///// Set global style /////
ROOT.gROOT.SetStyle('Plain');
ROOT.gStyle.SetPalette(56); # 51:blue. 52:lo=k/hi=w. 53:lo=r/hi=y. 56:lo=y/hi=r. 54:lo=b/hi=y. 55:lo=b/hi=r, pale rainbow. none:rainbow 
# Palettes, see https://root.cern.ch/doc/master/classTColor.html#C05. Or for ROOT 5.34: https://people.nscl.msu.edu/~noji/colormap
#// Boxes
ROOT.gStyle.SetLegendFont(132);
ROOT.gStyle.SetStatFont(132);
#// Histogram titles
ROOT.gStyle.SetTitleFont(132, 'h');
ROOT.gStyle.SetTitleSize(0.05, 'h'); #0.06
ROOT.gStyle.SetTitleBorderSize(0);
#// Axis titles
ROOT.gStyle.SetTitleFont(132, 'xyz');
ROOT.gStyle.SetTitleSize(0.04, 'xyz'); #0.04
ROOT.gStyle.SetTitleOffset(1.25, 'x'); #1.15
ROOT.gStyle.SetTitleOffset(1, 'y'); #0.7
#// Axis labels
ROOT.gStyle.SetLabelFont(132, 'xyz');
ROOT.gStyle.SetLabelSize(0.04, 'xyz');
#// Text options
ROOT.gStyle.SetTitleFont(132, 't');
ROOT.gStyle.SetTextFont(132);
ROOT.gStyle.SetTextSize(0.025)
ROOT.gStyle.SetPaintTextFormat("3.2g");
#// Legend text size
#ROOT.gStyle.SetLegendTextSize(0.1)
#// Border options
ROOT.gStyle.SetCanvasBorderSize(0);
ROOT.gStyle.SetFrameBorderSize(0);
ROOT.gStyle.SetLegendBorderSize(0);  #0,1
ROOT.gStyle.SetStatBorderSize(0);
ROOT.gStyle.SetTitleBorderSize(0);
#// Fit options
#ROOT.gStyle.SetOptFit()
ROOT.gROOT.ForceStyle();







### From https://en.wikipedia.org/wiki/Standard_drink
WaterDensity = 1 						# g/mL
AlcoholDensity = 0.789						# Ethanol, g/mL

StandardServing_g_US = 14 					# g


### From https://alcohol.stackexchange.com/questions/1064/by-volume-by-weight-conversion-formula
ABV = float(raw_input('ABV = '))/100.				# Alcohol By Volume, alcohol mL / total mL * 100
WBV = 1-ABV
TotalWeight = ABV*AlcoholDensity + WBV*WaterDensity		# 
ABW = AlcoholDensity*ABV/TotalWeight				# 
ServingWeight = StandardServing_g_US/ABW			#
print ' ', round(ServingWeight,2), 'g for standard', StandardServing_g_US, 'g serving of alcohol at', str(round(ABV*100,2))+'% AVB.'


maxabv = 100

'''
print 'ABV (%) \t Mass (g)'
for i in range(1,maxabv+1):
  abv = i/100.
  wbv = 1-abv
  tw = abv*AlcoholDensity + wbv*WaterDensity
  print abv*100,'\t', StandardServing_g_US/(AlcoholDensity*abv/tw)
'''

abv = [float(i) for i in range(1,maxabv+1)]
mass_per_abv =[round(StandardServing_g_US/(AlcoholDensity*(i/100.)/((i/100.)*AlcoholDensity+(1-(i/100.))*WaterDensity)),2) for i in range(1,maxabv+1)]

# Primed printout for markdown table
print 'ABV (%) | Mass (g)'
print '--- | ---'
for a,m in zip(abv,mass_per_abv):
  print a,'|',m


c = ROOT.TCanvas('c', 'c', 1366, 720)
c.SetTicks()
c.SetGrid()
c.SetLogx()
c.SetLogy()
g = ROOT.TGraph(len(abv), np.array(abv), np.array(mass_per_abv))
g.SetTitle(';AVB (%);Total mass (g)')
g.Draw('alp')
c.SaveAs('StandardServingOfAlcohol.png')

